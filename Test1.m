clc; close all; clear

%% carag senal

load('ecgConditioningExample.mat', 'ecg')
load('ecgConditioningExample.mat', 'fs')

secg = ecg;

%% segla de ecg
G = 2000;


ecg_mv = secg/G;

figure;
plot(ecg_mv);
ylabel('amplitud mV');
xlim([1 1000])


%% Vector Tiempo

Fs = 1000; %hz
Ts = 1/Fs; %s %periodo de mostreo

N = length(secg);
vect  = (1:1:N);
T = vect*Ts; 

figure;
plot(T,ecg_mv);
ylabel('amplitud mV');
xlim([0 4])

%% centrar la señal en 0

% voy a restar a cada valor el promedio de toda la señal 

ecg_final = (ecg_mv - mean(ecg_mv))/std(ecg_mv);

figure;
plot(T,ecg_final);
xlabel('Tiempo (s)')
ylabel('Amplitud (mV)')
title ('ECG')
xlim([0 4])



%% Filtro Fir

%caracterirsticas del filtro

orden = 200;

%limites inferior y superior
limi = 58
lims = 61

%normalizo
limi_n = limi/(Fs/2);
lims_n = lims/(Fs/2);

%creo filtro
a = 1 ;
b = fir1(200, [limi_n lims_n], 'stop');

%filtro la señal

ecg_limp = filtfilt (b,a,ecg_final);

figure;
plot(T,ecg_limp);
title ('ECG, filtro interferencia linea eletrica')
xlim([0 4])

%% filterDesigner 60Hz

Fs = 1000;  % Sampling Frequency

N1   = 10;  % Order
Fc1 = 59;  % First Cutoff Frequency
Fc2 = 61;  % Second Cutoff Frequency

% Construct an FDESIGN object and call its BUTTER method.
h  = fdesign.bandstop('N,F3dB1,F3dB2', N1, Fc1, Fc2, Fs);
Hd = design(h, 'butter');

%filtro la señal

ecg_limp2 = filter (Hd,ecg_final);

figure;
plot(T,ecg_limp2);
title ('ECG, filtro interferencia linea eletrica')
xlim([0 4])


%% 0.5Hz

Fs = 1000;  % Sampling Frequency

N2     = 20;   % Order
Fpass = 0.5;  % Passband Frequency
Apass = 1;    % Passband Ripple (dB)
Astop = 80;   % Stopband Attenuation (dB)

% Construct an FDESIGN object and call its ELLIP method.
h2  = fdesign.highpass('N,Fp,Ast,Ap', N2, Fpass, Astop, Apass, Fs);
Hd2 = design(h2, 'ellip');

ecg_limp3 = filter (Hd2,ecg_limp2);

figure;
plot(T,ecg_limp3);
title ('ECG, filtro pasa alto')
xlim([0 4])

%% filtro pasa bajo

Fs = 1000;  % Sampling Frequency

Fpass = 90;              % Passband Frequency
Fstop = 100;             % Stopband Frequency
Dpass = 0.057501127785;  % Passband Ripple
Dstop = 0.0001;          % Stopband Attenuation
dens  = 20;              % Density Factor

% Calculate the order from the parameters using FIRPMORD.
[N3, Fo, Ao, W] = firpmord([Fpass, Fstop]/(Fs/2), [1 0], [Dpass, Dstop]);

% Calculate the coefficients using the FIRPM function.
b1  = firpm(N3, Fo, Ao, W, {dens});
Hd3 = dfilt.dffir(b1);

ecg_limp4 = filter (Hd3,ecg_limp3);

figure;
plot(T,ecg_limp4);
title ('ECG, filtro pasa bajo')
xlim([0 4])